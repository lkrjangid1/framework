/**
 * Manages the Heatmap, retrives data from server and constructs pagination and tables for index.cpt of Viz Controller
 * @author Utsav Munendra
 */

/**
 * To minimize the number of globals exposed, all functions and variables
 * are a property of this object, styled similarly to the L in Leaflet library.
 * @namespace
 */
var H = {

    /**
     * An object describing a particular provenience.
     * @typedef {Object} pItem
     * @property {number} id - Provenience ID
     * @property {string} name - Provenience name
     * @property {string} location - Provenience location with longitute and then latitude
     * @property {number} aCount - Number of artifacts associated with this provenience
     */

    /**
     * Stores the provenience data downloaded from the servers with the applied filters.
     * Data may have been processed and may not be exactly the one downloaded from server.
     * @type {pItem[]}
     */
    data: [],

    /**
     * The Heat Layer Object for the heatmap and not the associated tiles, map box and pins.
     * @type {Object}
     */
    heatMap: null,

    /**
     * An L.map Object for the map box.
     * @type {Object}
     */
    mapViz: null,

    /**
     * It is false until the heatmap has initialized for the very first time, and it is true afterwards.
     * @type {boolean}
     */
    mapInitialized: false,

    /**
     * An object whose keys are provenience ids and their property is a Leaflet map marker
     * @type {Object}
     */
    mapMarkers: {},

    /**
     * An L.TileLayer object which represents the tiles on the map
     * @type {Object}
     */
    tiles: null,

    /**
     * Total number of rows in the table which will have processed data downloaded from the server.
     * @type {number}
     */
    totalTableRows: 0,

    /**
     * Rows of the table that would be shown at once to the user.
     * @type {number}
     */
    rowsShownAtOnce: 10,

    /**
     * The page number currently displayed to user.
     * @type {number}
     */
    currentPage: 1,



    /**
     * Attaches event listeners to various elements.
     */
    fixEventListeners: function () {
        //Uncomment when developing filter
        //$('#filter-modal').modal('show');

        //Pagination event handlers
        document.getElementById('page-first').addEventListener('click', function (e) { e.preventDefault(); H.updatePage(1) });
        document.getElementById('page-prev').addEventListener('click', function (e) { e.preventDefault(); H.updatePage(Number.parseInt(H.currentPage) - 3); });
        document.getElementById('page-1').addEventListener('click', H.paginationClick);
        document.getElementById('page-2').addEventListener('click', H.paginationClick);
        document.getElementById('page-3').addEventListener('click', H.paginationClick);
        document.getElementById('page-next').addEventListener('click', function (e) { e.preventDefault(); H.updatePage(Number.parseInt(H.currentPage) + 3); });
        document.getElementById('page-last').addEventListener('click', function (e) { e.preventDefault(); H.updatePage(Infinity); });

        //Event listeners for Map options
        let minOpacityLabel = document.getElementById('min-opacity-label');
        let minOpacitySlider = document.getElementById('min-opacity-slider');

        minOpacitySlider.addEventListener('input', function () { H.setOpacity(minOpacityLabel, minOpacitySlider.value); });
        document.getElementById('reset-map').addEventListener('click', function (e) { e.preventDefault(); H.resetMap(minOpacityLabel, minOpacitySlider); });
        document.getElementById('map-tiles').addEventListener('change', function (e) { H.changeMapTiles(e.target.options[e.target.selectedIndex]); });
        document.getElementById('map-options-pin').addEventListener('click', function (e) { e.preventDefault(); H.pinProveniences(); });
        document.getElementById('map-options-heatmap').addEventListener('change', function (e) { H.toggleHeatMap(e.target.checked); });

        //Microsoft Edge does not apply .custom-range Bootstrap CSS class correctly to sliders in versions at least 44
        //In that case, that class is removed.
        if (/Edge/.test(window.navigator.userAgent)) {
            minOpacitySlider.classList.remove('custom-range');
            minOpacitySlider.classList.add('d-block', 'w-100');
        }
    },



    /**
     * Manages sending a POST request to the server. 
     * To get filtered data, payload must be in the form of { material: [1, 6, 8...], collection: [8], ...}.
     * Requires an HTMLElement with ID 'info-element' and dttributes data-url having the filter request URL and
     * a data-csrf with the CSRF Token in it.
     * @param {Object} payload Data to be sent to the server
     */
    sendPost: function (payload) {
        const infoElement = document.getElementById('info-element');
        var xhr = new XMLHttpRequest();

        ///Fixing event listeners
        xhr.addEventListener('load', function () { H.XHRLoadHandler(this.response); });
        xhr.addEventListener('error', H.XHRErrorHandler);

        //Setting headers
        xhr.open('POST', infoElement.dataset.url);
        xhr.responseType = 'json';
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-CSRF-Token', infoElement.dataset.csrf);

        //Sending the request
        xhr.send(JSON.stringify(payload));

        //Adding a loading sign to the map box if it has initialized
        if (H.mapViz) {
            H.mapViz.attributionControl.setPrefix("Loading...");
        }
    },



    /**
     * Invokes when there is an error retrieving the filter.
     */
    XHRErrorHandler: function () {
        //Informing about the error through the mapbox if it has initialized
        if (H.mapViz) {
            H.mapViz.attributionControl.setPrefix("Request for data could not be sent");
        }
    },



    /**
     * Invokes when the response for the POST request for data is successfully received from the server.
     * @param {pItem[]} response The response from the server
     */
    XHRLoadHandler: function (response) {
        //In case the response is empty
        H.data = (response) ? response : [];

        //Removing the loading sign from the map box
        if (H.mapViz) {
            H.mapViz.attributionControl.setPrefix('');
        }
        H.totalTableRows = H.data.length;

        //Displaying the initial data with the table
        H.plot();
        H.updatePage(1);
    },



    /**
     * Inserts comma and transforms the number according to the locale of the user.
     * @param {number} num A number to be made more readable.
     * @returns {string} Transformed number
     */
    commaNum: function (num) {
        //Checking if the function is available in user's browser
        if (typeof Intl == 'object' && Intl && typeof Intl.NumberFormat == 'function') {
            return num.toLocaleString();
        }
        return num;
    },



    /**
     * Plots the heatmap on the map box and initialized the mapbox if this is the first time.
     */
    plot: function () {

        /** Top row will have provenience id of null denoting the number of artifacts without a provenience. */
        let noProvNum = 0;
        if (H.data[0] !== undefined && H.data[0].pId === null) {
            noProvNum = Number(H.data.shift().aCount);
            --H.totalTableRows;
        }

        /** Number of artifacts which have a provenience but not the location of the provenience. */
        let provUnknown = 0;
        /** Number of artifacts of the provenience with the most artifacts. */
        let maxACount = 0;
        /** Total number of provenience with no null entries  */
        let totalIdCount = 0;
        /** Array to be passed to the HeatLayer plugin */
        let preppedData = [];

        // Processing each element in H.data or the receiving data from the server
        H.data.forEach(function (item) {
            /** Artifact count of the item. */
            const aCount = Number(item.aCount);

            if (item.location == null) {
                //When provenience location is unknown
                provUnknown += aCount;
                totalIdCount += aCount;

            } else {
                //When provenience location is known
                const loc = item.location.split(',');
                let mapElement = [Number(loc[1]), Number(loc[0]), aCount];

                //If a location is given and we fail to parse the location, then we skip this provenience
                if (isNaN(mapElement[0])) {
                    return;
                }

                //Then the provenience will be displayed on the heatmap
                preppedData.push(mapElement);

                totalIdCount += aCount;
                if (aCount > maxACount) { maxACount = aCount; }
            }
        });

        // Informing the user in a banner alert about the artifacts with no proveniences
        let alertNoProvNum = document.getElementById('alert-no-prov-num');
        let percent = noProvNum / (totalIdCount + noProvNum) * 100;
        if (alertNoProvNum) {

            //Yellow Display with exclamation sign
            if (noProvNum) {
                alertNoProvNum.classList.remove('alert-info');
                alertNoProvNum.classList.add('alert-warning');
                alertNoProvNum.firstElementChild.innerHTML = (noProvNum === 1) ?
                    //Singular
                    '<i class="fa fa-exclamation-circle"></i> <strong>' + H.commaNum(noProvNum) + '</strong> (' + percent.toFixed(1)
                    + '%) artifact does not have an associated provenience.' :
                    //Plural
                    '<i class="fa fa-exclamation-circle"></i> <strong>' + H.commaNum(noProvNum) + '</strong> (' + percent.toFixed(1)
                    + '%) artifacts do not have an associated provenience.';

                //Blue display for when everything is all right
            } else {
                alertNoProvNum.classList.remove('alert-warning');
                alertNoProvNum.classList.add('alert-info');
                alertNoProvNum.firstElementChild.innerHTML =
                    '<i class="fa fa-check-circle"></i> <strong>All</strong> artifacts have an associated provenience.';
            }
        }

        // Informing the user in a banner alert about the artifacts with unknown provenience locations
        let alertUnknown = document.getElementById('alert-unknown');
        percent = provUnknown / (totalIdCount + noProvNum) * 100;
        if (alertUnknown) {

            //Yellow display with exclamation sign
            if (provUnknown) {
                alertUnknown.classList.remove('alert-info');
                alertUnknown.classList.add('alert-warning');
                alertUnknown.firstElementChild.innerHTML = (provUnknown === 1) ?
                    //Singular
                    '<i class="fa fa-exclamation-circle"></i> The provenience of <strong>' + H.commaNum(provUnknown) + '</strong> (' + percent.toFixed(1)
                    + '%) artifact does not have a known location.' :
                    //Plural
                    '<i class="fa fa-exclamation-circle"></i> The proveniences of <strong>' + H.commaNum(provUnknown) + '</strong> (' + percent.toFixed(1)
                    + '%) artifacts do not have a known location.';

                //Blue display for when everything is all right
            } else {
                alertUnknown.classList.remove('alert-warning');
                alertUnknown.classList.add('alert-info');
                alertUnknown.firstElementChild.innerHTML = '<i class="fa fa-check-circle"></i> The proveniences of <strong>all</strong> the artifacts have a known location.';
            }
        }


        // Determining the intensity of each provenience point which is between 0 and 1
        preppedData.forEach(function (item) {
            item[2] /= maxACount;
        });


        if (!H.mapInitialized) {

            //When map box is being initialized for the first time.
            H.mapInitialized = true;
            const initialCoords = (preppedData.length > 0 && !preppedData[0][1]) ? [preppedData[0][0], preppedData[0][1]] : [34, 43];

            //Setting the initial view point and zoom of the map.
            H.mapViz = L.map('map-viz').setView(initialCoords, 4);

            //Autofilling coordinates when clicked on the map
            H.mapViz.addEventListener('click', function (e) { H.autoFillCoords(e.latlng); });

            //Storing the tiles of the map
            H.tiles = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
                subdomains: 'abcd',
                maxZoom: 19
            }).addTo(H.mapViz);

            //Distance Scale
            L.control.scale().addTo(H.mapViz);

            //Full screen button
            H.mapViz.addControl(new L.Control.Fullscreen());

            //Storing the heat layer
            H.heatMap = L.heatLayer(preppedData, { radius: 30, maxZoom: 6, minOpacity: 0.33 }).addTo(H.mapViz);

        } else {
            //Removing the existing layer and adding a new heat layer
            H.mapViz.removeLayer(H.heatMap);
            H.heatMap = L.heatLayer(preppedData, { radius: 30, maxZoom: 6, minOpacity: 0.33 }).addTo(H.mapViz);
        }
    },



    /**
     * Constructs and populates the table displayed below the heatmap.
     */
    fillTable: function () {

        /** Column names */
        const attributes = ['ID', 'Provenience Name', 'Number of Artifacts', 'Actions'];

        //Emptying Table head
        const thead = document.getElementById('data-thead');
        while (thead.hasChildNodes()) {
            thead.removeChild(thead.lastChild);
        }

        //Making new table head
        const headRow = document.createElement('tr');
        attributes.forEach(function (item) {
            const cell = document.createElement('th');
            cell.appendChild(document.createTextNode(item));
            headRow.appendChild(cell);
        });
        thead.appendChild(headRow);

        //Emptying table rows
        const tbody = document.getElementById('data-tbody');
        while (tbody.hasChildNodes()) {
            tbody.removeChild(tbody.lastChild);
        }

        //Making new table rows
        if (H.totalTableRows > 0) {
            let pageIndex = (H.currentPage - 1) * H.rowsShownAtOnce;
            let i = Math.min(H.rowsShownAtOnce, H.totalTableRows - pageIndex);

            while (i-- > 0) {
                let bodyRow = document.createElement('tr');

                let cell = [];
                for (let i = 0; i < 4; ++i) {
                    cell[i] = document.createElement('td');
                    cell[i].classList.add('align-middle');
                }
                let dataItem = H.data[pageIndex];

                //First cell has the Provenience ID
                cell[0].appendChild(document.createTextNode(dataItem.pId));
                cell[0].classList.add('border-right');

                //Fouth cell has the action buttons
                let pinButton = document.getElementById('action-pin').cloneNode(true);
                pinButton.classList.remove('d-none');
                pinButton.removeAttribute('id');
                pinButton.addEventListener('click', function () { H.pinOnMap(dataItem) });

                let searchButton = document.getElementById('action-search').cloneNode(true);
                searchButton.classList.remove('d-none');
                searchButton.removeAttribute('id');
                searchButton.href += '?Provenience=' + dataItem.pId;
                cell[3].appendChild(pinButton);
                cell[3].appendChild(searchButton);

                //Second cell has the provenience name
                if (dataItem.name) {
                    cell[1].appendChild(document.createTextNode(dataItem.name));
                } else {
                    cell[1].innerHTML = '<em class="text-muted">Provenience name not in database</em>';
                }

                //Third cell has the artifact count
                if (!dataItem.location) {
                    cell[1].innerHTML += '<br/><em class="text-muted">Provenience location unknown</em>';
                    pinButton.setAttribute('disabled', true);
                    pinButton.firstElementChild.classList.remove('fa-map-marker');
                    pinButton.firstElementChild.classList.add('fa-question');
                }
                cell[2].appendChild(document.createTextNode(dataItem.aCount));

                cell.forEach(function (item) { bodyRow.appendChild(item); });
                tbody.appendChild(bodyRow);
                ++pageIndex;
            }

            //If there are no records to show in the table
        } else {
            let bodyRow = document.createElement('tr');
            let cell = document.createElement('td');
            cell.innerHTML = '<em class="text-muted">No such records found.</em>';
            cell.setAttribute('colspan', attributes.length);
            bodyRow.appendChild(cell);
            tbody.appendChild(bodyRow);
        }
    },



    /**
     * Constructs a new table of the told page number.
     * @param {number} pageNum The page number
     */
    updatePage: function (pageNum) {
        /** Total number of pages according to the data. */
        let numPages = Math.ceil(H.totalTableRows / H.rowsShownAtOnce);

        let pages = [];
        pages[0] = document.getElementById('page-1');
        pages[1] = document.getElementById('page-2');
        pages[2] = document.getElementById('page-3');

        //Adjusting page number to be within bounds
        H.currentPage = (pageNum < 1) ? 1 : ((pageNum > numPages) ? numPages : pageNum);

        //When there are no pages possbile
        if (numPages == 0) {
            H.currentPage = 0;

            //Only dashes are shown
            pages[0].classList.remove('active'); pages[1].classList.add('active'); pages[2].classList.remove('active');

            for (let i = 0; i < 3; ++i) {
                pages[i].firstChild.innerHTML = '-';
            }

            //When only one page is possible
        } else if (numPages == 1) {
            H.currentPage = 1;
            pages[0].classList.remove('active'); pages[1].classList.add('active'); pages[2].classList.remove('active');
            pages[0].firstChild.innerHTML = pages[2].firstChild.innerHTML = '-';
            pages[1].firstChild.innerHTML = '1';

            //When two pages are possible
        } else if (numPages == 2) {
            pages[0].firstChild.innerHTML = '1'; pages[1].firstChild.innerHTML = '2'; pages[2].firstChild.innerHTML = '-';
            if (H.currentPage == 1) {
                pages[0].classList.add('active'); pages[1].classList.remove('active'); pages[2].classList.remove('active');
            } else {
                pages[0].classList.remove('active'); pages[1].classList.add('active'); pages[2].classList.remove('active');
            }

            //When three or more pages are possible
        } else {
            let firstShownPage = H.currentPage - 1;

            //Current page is the first of the three buttons shown
            if (H.currentPage == 1) {
                firstShownPage = 1;
                pages[0].classList.add('active'); pages[1].classList.remove('active'); pages[2].classList.remove('active');

                //Current page is the last of the three buttons shown
            } else if (H.currentPage == numPages) {
                firstShownPage = numPages - 2;
                pages[0].classList.remove('active'); pages[1].classList.remove('active'); pages[2].classList.add('active');

                //Current page is the middle one of the three buttons shown
            } else {
                pages[0].classList.remove('active'); pages[1].classList.add('active'); pages[2].classList.remove('active');
            }

            //Adding the page number to the buttons
            for (let i = 0; i < 3; ++i) {
                pages[i].firstChild.innerHTML = firstShownPage + i;
            }
        }

        H.fillTable();
    },



    /**
     * Invokes when any one of the three numbered pagination buttons is clicked.
     * Updates the table page according to the number in the clicked button.
     * @param {MouseEvent} e Information about the click event
     */
    paginationClick: function (e) {
        e.preventDefault();
        const functionParam = Number.parseInt(e.target.firstChild.textContent);
        if (!Number.isNaN(functionParam)) {
            H.updatePage(e.target.firstChild.textContent);
        }
    },



    /**
     * Places a pin on the told provenience
     * @param {pItem} item Contains information about the provenience whose pin is to be shown
     */
    pinOnMap: function (item) {
        //Deletes if a pin already exists
        H.deletePinOnMap(item.pId);

        //Determining the location of the provenience
        let loc = item.location.split(',');
        H.mapMarkers[item.pId] = L.marker([Number(loc[1]), Number(loc[0])]);

        //Constructing the popup of the pin
        H.mapMarkers[item.pId].bindPopup('<strong>' + item.name + '</strong><br/>Artifacts: ' + item.aCount
            + '<br/><a href="#" onclick="H.deletePinOnMap(' + item.pId + '); return false;">Remove Pin</a>', {
                'className': 'map-popup'
            });

        //Placing the pin and panning to it
        H.mapMarkers[item.pId].addTo(H.mapViz);
        H.mapViz.panTo([Number(loc[1]), Number(loc[0])]);
    },



    /**
     * Deletes a specific pin or all the pins on the map
     * @param {boolean|number} id Provenience ID of the pin to be removed. If false, then all pins are removed
     */
    deletePinOnMap: function (id = false) {
        //Deleting a specific pin
        if (id) {
            if (H.mapMarkers[id] !== undefined) {
                //If pin exists, it is removed from map and H.mapMarkers
                H.mapViz.removeLayer(H.mapMarkers[id]);
                delete H.mapMarkers[id];
            }

            //Deleting all pins
        } else {
            for (let pin in H.mapMarkers) {
                if (H.mapMarkers.hasOwnProperty(pin)) {
                    H.mapViz.removeLayer(H.mapMarkers[pin]);
                }
            }
            H.mapMarkers = {};
        }
    },



    /**
     * Sets the opacity of the heatmap
     * @param {HTMLElement} label Label of the slider 
     * @param {number} num Value of the slider
     */
    setOpacity: function (label, num) {
        label.innerHTML = "<strong>Minimum Opacity:</strong> " + num + "%";

        //If heatmap is initialized, it is set to the default values
        if (H.heatMap) {
            H.heatMap.setOptions({ minOpacity: num / 100 });
        }
    },



    /**
     * Resets the map to default value
     * @param {HTMLElement} label Label of the slider
     * @param {HTMLElement} slider Slider for setting the opacity of the map
     */
    resetMap: function (label, slider) {
        //Resetting the opacity
        H.setOpacity(label, 33);
        slider.value = 33;

        //Resetting the view
        H.mapViz.setView([34, 43], 4);

        //Deleting all the pins on the map
        H.deletePinOnMap();
    },



    /**
     * Changes the tiles of the heatmap
     * @param {HTMLElement} tileOption The selected option with the tiles information
     */
    changeMapTiles: function (tileOption) {
        let mapOptions = { attribution: tileOption.dataset.attribution, maxZoom: 19 };

        //If this map has subdomains, then those are added
        if (tileOption.dataset.subdomains)
            mapOptions.subdomains = tileOption.dataset.subdomains;

        //Previous tile layer is removed and a new one is added
        H.mapViz.removeLayer(H.tiles);
        H.tiles = L.tileLayer(tileOption.dataset.url, mapOptions).addTo(H.mapViz);
    },



    /**
     * Fills in the latitute, longitute and an estimated radius into the map options form
     * @param {Object} location A Leaflet LatLng object
     */
    autoFillCoords: function (location) {
        //Setting the latitute and longitute
        document.getElementById('map-option-lat').value = location.lat.toFixed(2);
        document.getElementById('map-option-lng').value = location.lng.toFixed(2);

        //Length and breadth of the displayed map in geo-coordinates
        let diffLat = Math.abs(H.mapViz.getBounds().getNorth() - H.mapViz.getBounds().getSouth());
        let diffLng = Math.abs(H.mapViz.getBounds().getEast() - H.mapViz.getBounds().getWest());

        //Identifying a new point on the circumference of the chosen circle
        let centerPoint = H.mapViz.getCenter();
        let circumPoint = L.latLng(centerPoint.lat + Math.min(diffLat, diffLng) * 0.25, centerPoint.lng);

        //Distance is the quarter of the smaller of the height and width of the map in km
        let distance = circumPoint.distanceTo(centerPoint) / 1000;

        //Setting the radius
        document.getElementById('map-option-radius').value = distance.toFixed(2);
    },



    /**
     * Invokes when the user wants to see pins near a coordinate
     */
    pinProveniences: function () {
        //Getting values from the form
        let pinLat = document.getElementById('map-option-lat');
        let pinLng = document.getElementById('map-option-lng');
        let pinRadius = document.getElementById('map-option-radius');

        //Setting default values if form is partially filled
        if (!pinLat.value) { pinLat.value = 0; }
        if (!pinLng.value) { pinLng.value = 0; }
        if (!pinRadius.value) { pinRadius.value = 0; }

        //Adding pins if data has been loaded
        if (H.data && H.data.length > 0) {

            /** Radius in meters */
            let radius = Number(pinRadius.value) * 1000;

            /** Center point chosen by the user as a Leaflet LatLng object */
            let pin = L.latLng(pinLat.value, pinLng.value);

            //Processing each provenience on the map which has a location
            H.data.forEach(function (item) {
                if (item.location) {
                    //Finding the location of the provenience
                    let loc = item.location.split(',');
                    let itemLoc = L.latLng(loc[1], loc[0]);

                    //Identifying if the provenience is within the radius
                    if (pin.distanceTo(itemLoc) < radius) {
                        H.pinOnMap(item);
                    }
                }
            });
        }
    },



    /**
     * Toggles showing of the heatmap over the chosen map tiles in the Leaflet map box
     * @param {boolean} show Pass true when heatmap is to be shown, false otherwise
     */
    toggleHeatMap: function (show = true) {
        //Toggling only if the heat layer is processed and drawn
        if (H.heatMap && H.mapViz) {
            show ? H.mapViz.addLayer(H.heatMap) : H.mapViz.removeLayer(H.heatMap);
        }
    },
};



//Initializing
H.fixEventListeners();

//Getting all the data for initial display
H.sendPost();

//Exposing sendPost for filter file
window.sendPost = H.sendPost;
