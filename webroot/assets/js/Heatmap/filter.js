/**
 * @file Manages filters by sending POST request to server, storing the active filters and by implementing filter related front-end
 * @requires an info-element ID for F.getFilter, filterNames for F.filters, sendPost() for F.applyFilter
 * @author Utsav Munendra 
 */


/**
 * To minimize the number of globals exposed, all functions and variables
 * are a property of this object, styled similarly to the L in Leaflet library.
 * @namespace
 */
var F = {

    /**
     * Stores the code-names of all the filters available.
     * A line like this is provided by filter PHP code in index.cpt: const filterNames = ['material', 'collection', ...],
     * and that is available here via this variable.
     * All IDs in HTML code related to filters have these code-names party.
     * @type {string[]} 
     */
    filters: filterNames || [],

    /**
     * An object describing a particular filter entry.
     * @typedef {Object} item
     * @property {number} id - ID of the filter entry
     * @property {string} filter - Filter name
     * @property {number} aCount - Number of artifacts associated with this filter entry alone
     */

    /**
     * Stores the downloaded filters.
     * @type {Object}
     * @property {item[]} An array of items is the property and elements from F.filters are the key
     */
    filterOptions: {},


    /**
     * Stores the filters which are selected.
     * @type {Object} 
     * @property {item[]} An array of items is the property and elements from F.filters are the key
     */
    selectedFilters: {},



    /**
     * Attaches event listeners to the appropriate elements
     */
    fixEventListeners: function () {
        // Handling the up/down arrows of the filter groups
        F.filters.forEach(function (item) {
            $('#filter-' + item)
                .on('show.bs.collapse', function () { F.openFilter(item); })
                .on('hide.bs.collapse', function () { F.closeFilter(item); });
        });

        //Apply button of the filter
        document.getElementById('filter-button-apply').addEventListener('click', F.applyFilter);
    },



    /**
     * Invokes when a filter group is opened and populates it if needed
     * @param {String} item An element of F.filters representing the filter group
     */
    openFilter: function (item) {
        const iconElement = document.getElementById('filter-' + item + '-icon');

        //Downloaded the filter tags if needed
        if (F.filterOptions[item] == undefined) { F.getFilter(item); }

        //Toggles up/down arrow
        iconElement.classList.remove('fa-angle-down');
        iconElement.classList.add('fa-angle-up');
    },



    /**
     * Invokes when a filter group is closed
     * @param {string} item An element of F.filters representing the filter group
     */
    closeFilter: function (item) {
        const iconElement = document.getElementById('filter-' + item + '-icon');
        iconElement.classList.add('fa-angle-down');
        iconElement.classList.remove('fa-angle-up');
    },



    /**
     * Downloads the tags in a filter group and populates it
     * Requires an HTMLElement with ID 'info-element' in the HTML which has data-url attribute set to the URL
     * in the server which server the filter and a data-csrf attribute with the CSRF Token
     * @param {string} filterName An element of F.filters representing the filter needed to be downloaded
     */
    getFilter: function (filterName) {
        const infoElement = document.getElementById('info-element');
        let xhr = new XMLHttpRequest();
        const loadingText = document.getElementById('filter-' + filterName + '-loading-text');

        //Initial text in the filter group while it is loading
        loadingText.innerHTML = 'Loading Filter <i class="fa fa-spinner rotation"></i>';

        //Fixing event handlers
        xhr.addEventListener('error', function () { F.XHRErrorHandler(loadingText, filterName); });
        xhr.addEventListener('load', function () { F.XHRLoadHandler(this.response, loadingText, filterName); });

        //Setting request header
        xhr.open('POST', infoElement.dataset.url);
        xhr.responseType = 'json';
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('X-CSRF-Token', infoElement.dataset.csrf);

        /***
         * When getting a filter, server expects the payload in the form of:
         * {
         *     getFilter: true,
         *     filter: 'material' (or any element in F.filters),
         * }
         */
        xhr.send(JSON.stringify({
            getFilter: true,
            filter: filterName
        }));
    },



    /**
     * Invokes when there is an error retrieving the filter
     * @param {HTMLElement} element An element which will show the error message
     * @param {string} filterName An element from F.filters representing the filter group
     */
    XHRErrorHandler: function (element, filterName) {
        if (element) {
            element.textContent = "Server received request for " + filterName + " but did not return anything. Try closing and reopening this filter group";
        }
    },



    /**
     * Invokes when the response for the POST request for filter is successfully received from the server
     * @param {item[]} response The response from the server when asking a filter
     * @param {HTMLElement|null} loadingText The element which will show the error message if server does not return anything, if needed
     * @param {string} filterName An element from F.filters representing the filter group
     */
    XHRLoadHandler: function (response, loadingText, filterName) {

        //When server returns nothing
        if (!response) {
            // Show an error message if possible and return immediately
            if (loadingText) {
                loadingText.textContent = "Server received request for " + filterName + " but did not return anything. Try closing and reopening this filter group";
            }
            return;
        }
        F.filterOptions[filterName] = response;

        //Dropdown menu
        const selectTag = document.getElementById('filter-' + filterName + '-select');

        //Div tag that holds the filter tag buttons
        const insertIn = document.getElementById('filter-' + filterName + '-body');
        insertIn.addEventListener('click', function (e) { F.filterTagClickHandler(e, filterName); });

        //Example of a tag button to be replicated
        const copyTag = insertIn.children[1].cloneNode(true);
        copyTag.classList.remove('d-none');

        //Clearing the tag holding div
        insertIn.innerHTML = '';

        if (selectTag) {
            //When the tag-names are long, a select element will be shown first
            selectTag.classList.remove('d-none');
            insertIn.appendChild(selectTag);

            //The top-most option of the select will have the value of -Infinity
            let firstOption = document.createElement('option');
            firstOption.setAttribute('selected', true);
            firstOption.innerHTML = "Choose a " + filterName;
            firstOption.value = -Infinity;
            selectTag.appendChild(firstOption);

            //Rest of the option will have the value of the filter id and the artifact count as data-a-count attribute
            F.filterOptions[filterName].forEach(function (item) {
                if (item.id != null && item.filter != null && item.aCount != null) {

                    //Creates a new option tag for the select element
                    let optionTag = document.createElement('option');
                    optionTag.value = item.id;
                    optionTag.dataset.aCount = item.aCount;
                    //Capitalizing first letter and adding artifact count in parenthesis
                    optionTag.innerHTML = item.filter.substring(0, 1).toUpperCase() + item.filter.substring(1) + ' (' + item.aCount + ')';
                    selectTag.appendChild(optionTag);

                } else {
                    //If some entries in the filter are null, those are reported to console for our manual inspection
                    console.log("Null entries in filter", filterName, item);
                }
            });

            //When an option of the select tag is selected, a corresponding tag button is added
            selectTag.addEventListener('change', function (e) {
                let tagId = Number(e.target.value);
                //Ignoring if the selected value is the first filler element
                if (tagId !== -Infinity) {

                    //Going over all the children of the filter group to determine if that option is already
                    //  present as a tag button
                    let addButton = true, numChild = insertIn.children.length;
                    for (let i = 0; i < numChild; ++i) {
                        let child = insertIn.children[i];
                        if (child.dataset.filterNum == tagId) {
                            addButton = false;
                            child.click();
                        }
                    }

                    //Ignoring if the tag button is already present in the body
                    if (addButton) {
                        let tagName = selectTag.selectedOptions[0].textContent;
                        let item = {
                            id: tagId,
                            //Removing the artifact count in the parenthesis from the option name
                            filter: tagName.substring(0, tagName.lastIndexOf('(')),
                            aCount: selectTag.selectedOptions[0].dataset.aCount,
                        };

                        //Adding the tag to the filter body
                        F.addTagToFilter(copyTag, insertIn, item, filterName, true);
                    }

                }
            });

        } else {
            //If tag names are concise, all the tags are added as soon as they are downloaded
            F.filterOptions[filterName].forEach(function (item) { F.addTagToFilter(copyTag, insertIn, item, filterName); });
        }

    },



    /**
     * Adds a filter entry as a tag to the filter body
     * @param {HTMLElement} copyTag A tag button which will be copied and inserted into insertIn
     * @param {HTMLElement} insertIn An element which will store all the copyTags
     * @param {item} item An object with the properties of a particular filter entry
     * @param {string} filterName An element of F.filters representing the filter group
     * @param {boolean?} clickIt Optional parameter to be set to true if the inserted tag should also be selected
     */
    addTagToFilter: function (copyTag, insertIn, item, filterName, clickIt = false) {

        //Ignoring if anything in the filter is null
        if (item.id != null && item.filter != null && item.aCount != null) {
            let copiedTag = copyTag.cloneNode(true);
            copiedTag.dataset.filterNum = item.id;
            copiedTag.dataset.filter = filterName;
            copiedTag.innerHTML = item.filter.substring(0, 1).toUpperCase() + item.filter.substring(1)
                + '<span class="artifact-count ml-2">' + item.aCount + '</span>';

            //Tooltip for the tag button with correct grammar when artifacts are singular or plural
            copiedTag.title = item.aCount + ' artifact' + ((item.aCount != 1) ? 's are' : ' is') + ' associated with this ' + filterName;
            insertIn.appendChild(copiedTag);

            if (clickIt) {
                copiedTag.click();
            }

        } else {
            //Reporting null entries for our manual inspection
            console.log("Null entries in filter", filterName, item);
        }
    },



    /**
     * Invokes when anything inside the filter body is clicked
     * @param {MouseEvent} e The event which occurs when the filter body is clicked
     * @param {string} filterName An element of F.filters representing the filter
     */
    filterTagClickHandler: function (e, filterName) {

        //Clicked element
        let tagElement = e.target;

        //If clicked inside the span of the tag button, then clicked element is changed from the span to the button
        if (e.target.tagName.toLowerCase() === 'span') {
            tagElement = e.target.parentElement;
        }

        let filterNumber = tagElement.dataset.filterNum;

        //Ignoring everything inside the filter body except the tag button
        if (filterNumber !== undefined) {

            //If selected filter has no filter ID from this type of filter, then the filter type and ID is added
            if (F.selectedFilters[filterName] === undefined) {
                F.selectedFilters[filterName] = [filterNumber];
                tagElement.classList.add('btn-primary');
                tagElement.classList.remove('btn-light');
            } else {
                let findIndex = F.selectedFilters[filterName].findIndex(function (num) { return num === filterNumber; });

                //If selected filter has IDs of this type but not this particular ID, then the ID is added
                if (findIndex === -1) {
                    F.selectedFilters[filterName].push(filterNumber);
                    tagElement.classList.add('btn-primary');
                    tagElement.classList.remove('btn-light');
                } else {

                    //If selected filter already has this ID, then it is removed
                    F.selectedFilters[filterName].splice(findIndex, 1);
                    tagElement.classList.remove('btn-primary');
                    tagElement.classList.add('btn-light');

                    //If after removing the ID, this filter type has no ID, then this type is removed from selected filter
                    if (F.selectedFilters[filterName].length === 0) {
                        delete F.selectedFilters[filterName];
                    }
                }
            }
        }
    },



    /**
     * Sends the selected filters to a function sending POST requests and closes the filter modal.
     * Requires a sendPost() function provided from outside this file which handles the data received from the server
     */
    applyFilter: function () {
        sendPost(F.selectedFilters);
        $('#filter-modal').modal('hide');
    }
};

F.fixEventListeners();