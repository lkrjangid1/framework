<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Staff Entity
 *
 * @property int $id
 * @property int $author_id
 * @property int $staff_type_id
 * @property string $cdli_title
 * @property string $contribution
 * @property string $sequence
 *
 * @property \App\Model\Entity\Author $author
 * @property \App\Model\Entity\StaffType $staff_type
 */
class Staff extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'author_id' => true,
        'staff_type_id' => true,
        'cdli_title' => true,
        'contribution' => true,
        'sequence' => true,
        'author' => true,
        'staff_type' => true
    ];
}
