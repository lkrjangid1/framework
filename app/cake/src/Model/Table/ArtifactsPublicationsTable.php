<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use ArrayObject;
use Cake\ORM\TableRegistry;

/**
 * ArtifactsPublications Model
 *
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsTo $Artifacts
 * @property \App\Model\Table\PublicationsTable|\Cake\ORM\Association\BelongsTo $Publications
 *
 * @method \App\Model\Entity\ArtifactsPublication get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArtifactsPublication newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArtifactsPublication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsPublication|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsPublication|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArtifactsPublication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsPublication[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArtifactsPublication findOrCreate($search, callable $callback = null, $options = [])
 */
class ArtifactsPublicationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('artifacts_publications');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Artifacts', [
            'foreignKey' => 'artifact_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Publications', [
            'foreignKey' => 'publication_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Convert input data to required format.
     *
     */
    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        // Removing leading and trailing whitespaces
        foreach ($data as $key => $value) {
            $data[$key] = trim($value);
        }

        // Conversion from publication bibtexkey to id
        $publications = TableRegistry::getTableLocator()->get('Publications');
        $publication = $publications->find('all', ['conditions' => ['bibtexkey' => $data['publication_id']]])->first();
        $data['publication_id'] = (isset($publication)) ? $publication->id:0;   // 0 indicates that the publication doesn't exist

        // Converting P# to artifact id
        if (!empty($data['artifact_id'])) {
            $data['artifact_id'] = empty(ltrim($data['artifact_id'], 'P0')) ? 0:ltrim($data['artifact_id'], 'P0');
        }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('artifact_id')
            ->nonNegativeInteger('artifact_id');

        $validator
            ->scalar('publication_id')
            ->nonNegativeInteger('publication_id');

        $validator
            ->scalar('exact_reference')
            ->maxLength('exact_reference', 20, 'The value cannot be more than 20 characters')
            ->allowEmpty('exact_reference');

        $validator
            ->scalar('publication_type')
            ->inList('publication_type', ['primary', 'electronic', 'citation', 'collation', 'history', 'other'], 'The publication type has to be one of the following: [primary, electronic, citation, collation, history, other]');

        $validator
            ->scalar('publication_comments')
            ->allowEmpty('publication_comments');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['artifact_id'], 'Artifacts', 'This id does not exist'));
        $rules->add($rules->existsIn(['publication_id'], 'Publications', 'This id does not exist'));

        $rules->add($rules->isUnique(
            ['publication_id', 'artifact_id', 'exact_reference', 'publication_type'],
            'This link already exists'
        ));

        $rules->add($rules->isUnique(
            ['artifact_id', 'publication_id', 'exact_reference', 'publication_type'],
            'This link already exists'
        ));

        $rules->add($rules->isUnique(
            ['exact_reference', 'artifact_id', 'publication_id', 'publication_type'],
            'This link already exists'
        ));

        $rules->add($rules->isUnique(
            ['publication_type', 'artifact_id', 'publication_id', 'exact_reference'],
            'This link already exists'
        ));

        return $rules;
    }
}
