<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal[]|\Cake\Collection\CollectionInterface $journals
 */

$title = [
    'cdlb' => 'Cuneiform Digital Library Bulletins',
    'cdlj' => 'Cuneiform Digital Library Journals',
    'cdln' => 'Cuneiform Digital Library Notes',
    'cdlp' => 'Cuneiform Digital Library Preprints'
][$type];
$hasHtml = $type != 'cdlp';
$hasPdf = $type != 'cdln';
?>

<h3 class="display-4 pt-3">
    <?= __($title) ?>
</h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead align="left">
        <tr>
            <th>No.</th>
            <th>Author</th>
            <th>Title</th>
            <th>Date</th>
            <?php if ($hasHtml): ?>
                <th>View</th>
            <?php endif; ?>
            <?php if ($hasPdf): ?>
                <th>File</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody align="left" class="journals-view-table">
        <?php foreach ($articles as $article):  ?>
            <tr>
                <td width="5%"><?= $article['serial']; ?></td>
                <td width="25%">
                    <?php foreach ($article['authors'] as $author):
                        $end = $author == end($article['authors']) ? '' : '; '; ?>
                        <a href="/authors/<?= $author['id'] ?>"><?=
                            $author['author']
                        ?></a><?= $end ?>
                    <?php endforeach; ?>
                </td>
                <td width="40%"><?= $article['title']; ?></td>
                <td width="15%"><?= $article['created']; ?></td>
                <?php if ($hasHtml): ?>
                    <td width="15%"><a href="<?= $article['article_type']; ?>/<?= $article['id']; ?>">view</a> </td>
                <?php endif; ?>
                <?php if ($hasPdf): ?>
                    <td width="15%"><a href="<?= $article['article_type']; ?>/<?= $article['id']; ?>.pdf">pdf</a> </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
