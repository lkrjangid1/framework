<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre $genre
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($genre) ?>
            <legend class="capital-heading"><?= __('Add Genre') ?></legend>
            <?php
                echo $this->Form->control('genre');
                echo $this->Form->control('parent_id', ['options' => $parentGenres, 'empty' => true]);
                echo $this->Form->control('genre_comments');
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Genres'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Parent Genres'), ['controller' => 'Genres', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Parent Genre'), ['controller' => 'Genres', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
