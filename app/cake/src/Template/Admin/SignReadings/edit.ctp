<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SignReading $signReading
 */
?>
<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <div class="signReadings form content">
            <?= $this->Form->create($signReading) ?>
            <fieldset>
                <legend><?= __('Edit Sign Reading') ?></legend>
                <?php
                    echo $this->Form->control('sign_name');
                    echo $this->Form->control('sign_reading');
                    echo $this->Form->control('preferred_reading');
                    echo $this->Form->control('period_id', ['options' => $periods, 'empty' => true]);
                    echo $this->Form->control('provenience_id', ['options' => $proveniences, 'empty' => true]);
                    echo $this->Form->control('language_id', ['options' => $languages, 'empty' => true]);
                    echo $this->Form->control('meaning');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <br/>
        <?= $this->Form->postLink(
                __('Delete Sign Reading'),
                ['action' => 'delete', $signReading->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $signReading->id), 'class' => 'btn-action']
            ) ?>
        <br/>
        <?= $this->Html->link(__('List Sign Readings'), ['controller' => 'SignReadings', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Periods'), ['controller' => 'Periods', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('New Period'), ['controller' => 'Periods', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Proveniences'), ['controller' => 'Proveniences', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('New Provenience'), ['controller' => 'Proveniences', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('New Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>
</div>
