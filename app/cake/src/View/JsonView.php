<?php
namespace App\View;

use Cake\ORM\Entity;
use Cake\Utility\Inflector;
use Cake\View\JsonView as CakeJsonView;

class JsonView extends CakeJsonView
{
    use SerializeTrait;

    protected $_responseType = 'json';

    protected function _serialize($serialize)
    {
        $data = $this->_dataToSerialize($serialize);
        $this->set('data', $this->prepareJson($data));

        return parent::_serialize('data');
    }

    private function prepareJson($data)
    {
        if ($data instanceof Entity) {
            return $this->prepareEntity($data);
        }

        if (is_array($data)) {
            foreach ($data as $key => $item) {
                if (is_array($item) || is_object($item)) {
                    $data[$key] = $this->prepareJson($item);
                } elseif (empty($item)) {
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }

    private function prepareEntity($entity)
    {
        $data = $entity->jsonSerialize();

        $repository = $entity->source();
        $key = Inflector::underscore(Inflector::singularize($repository));

        if (isset($data['_joinData'])) {
            $joinData = $this->prepareEntity($data['_joinData']);
            unset($data['_joinData']);
            $joinData[$key] = $this->prepareJson($data);
            return $joinData;
        } else {
            return $this->prepareJson($data);
        }
    }
}
